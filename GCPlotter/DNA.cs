﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCPlotter
{
    /// <summary>
    /// A base class which represents one DNA sequence as a list of nucleotides
    /// </summary>
    public class DNA : IEquatable<DNA>
    {
        /// <summary>
        /// Enum which represents one nucleotid
        /// </summary>
        public enum Nucleotid { A = 0, C = 1, G = 2, T = 3 };

        private List<Nucleotid> _dnaSequence;

        /// <summary>
        /// Public property which returns list of nucleotides who make this DNA sequence
        /// </summary>
        public List<Nucleotid> DNASequence
        {
            get
            {
                return _dnaSequence;
            }
        }

        /// <summary>
        /// Constructor wihtout arguments
        /// </summary>
        public DNA()
        {
            _dnaSequence = new List<Nucleotid>();
        }

        /// <summary>
        /// Consstructor with one argument
        /// </summary>
        /// <param name="dnaSequence">List of nucleotides</param>
        public DNA(List<Nucleotid> dnaSequence)
        {
            _dnaSequence = dnaSequence;
        }

        /// <summary>
        /// Constructor with one argument
        /// </summary>
        /// <param name="dnaSequence">String representation of nucleotides</param>
        public DNA(string dnaSequence)
        {
            _dnaSequence = new List<Nucleotid>();
            foreach (var n in dnaSequence)
            {
                switch (n)
                {
                    case 'A':
                    case 'a':
                        _dnaSequence.Add(Nucleotid.A);
                        break;
                    case 'C':
                    case 'c':
                        _dnaSequence.Add(Nucleotid.C);
                        break;
                    case 'G':
                    case 'g':
                        _dnaSequence.Add(Nucleotid.G);
                        break;
                    case 'T':
                    case 't':
                        _dnaSequence.Add(Nucleotid.T);
                        break;
                    case 'Y':
                    case 'y':
                        {
                            Random r = new Random();
                            _dnaSequence.Add((r.NextDouble() < 0.5) ? Nucleotid.T : Nucleotid.C);
                            break;
                        }
                    case 'R':
                    case 'r':
                        {
                            Random r = new Random();
                            _dnaSequence.Add((r.NextDouble() < 0.5) ? Nucleotid.A : Nucleotid.G);
                            break;
                        }
                    case 'n':
                    case 'N':
                        {
                            Random r = new Random();
                            double rand = r.NextDouble();
                            if(rand < 0.25)
                            {
                                _dnaSequence.Add(Nucleotid.A);
                            }
                            else if(rand < 0.5)
                            {
                                _dnaSequence.Add(Nucleotid.T);
                            }
                            else if(rand < 0.75)
                            {
                                _dnaSequence.Add(Nucleotid.C);
                            }
                            else
                            {
                                _dnaSequence.Add(Nucleotid.G);
                            }
                            break;
                        }
                    case '\r': case '\n': break;
                    default:
                        throw new Exception("Not supported character for conversion nucleotid");
                }
            }
        }

        /// <summary>
        /// Property which returns length of the DNA sequence
        /// </summary>
        public int Length
        {
            get
            {
                if (_dnaSequence != null)
                    return _dnaSequence.Count;

                return 0;
            }
        }

        /// <summary>
        /// ToString() implementation
        /// </summary>
        /// <returns>String representation of DNA sequence</returns>
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var n in _dnaSequence)
            {
                switch (n)
                {
                    case Nucleotid.A:
                        stringBuilder.Append('A');
                        break;
                    case Nucleotid.C:
                        stringBuilder.Append('C');
                        break;
                    case Nucleotid.G:
                        stringBuilder.Append('G');
                        break;
                    case Nucleotid.T:
                        stringBuilder.Append('T');
                        break;
                    default:
                        throw new Exception("Not supported character for conversion nucleotid");
                }
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Equality function
        /// </summary>
        /// <param name="other">Input DNA sequence</param>
        /// <returns>Wheter input DNA sequence equals to this DNA sequence</returns>
        public bool Equals(DNA other)
        {
            if (Object.ReferenceEquals(other, null))
            {
                return false;
            }

            if (Object.ReferenceEquals(this, other))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != other.GetType())
            {
                return false;
            }

            if (this.Length != other.Length) return false;

            for (int i = 0; i < this.Length; i++)
            {
                if (this._dnaSequence[i] != other._dnaSequence[i])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Equality function
        /// </summary>
        /// <param name="obj">Input object</param>
        /// <returns>Wheter input objects equals to this</returns>
        public override bool Equals(object obj)
        {
            return this.Equals(obj as DNA);
        }

        /// <summary>
        /// Hash function
        /// </summary>
        /// <returns>Hash value</returns>
        public override int GetHashCode()
        {
            int hash = 0;
            for (int i = 0; i < this.Length; i++)
            {
                hash += (int)_dnaSequence[i];
            }

            return hash;
        }
    }
}
