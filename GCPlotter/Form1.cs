﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace GCPlotter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        DNA sequence = null;

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                sequence = new FileGenomeLoader() { FileLocation = openFileDialog.FileName }.LoadGenome();
                int diff;
                int[] skew = DNAUtility.Skew(sequence, out diff);

                var minimas = DNAUtility.LocalMinimas(skew, skew.Length / 100, diff / 95).Distinct();
                DrawChart(skew, minimas);

                button1.Enabled = true;
                textBox1.Text = "";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            chart1.Series.Clear();
            chart1.Series.Add("GC-Diagram");
            chart1.Series["GC-Diagram"].ChartType = SeriesChartType.Line;
            chart1.Series["GC-Diagram"].ChartArea = "ChartArea1";

            WindowLengthLabel.Text = WindowTrackBar.Value.ToString();
            WindowTrackBar.ValueChanged += WindowTrackBar_ValueChanged;

            LWindowLengthLabel.Text = LWindowTrackBar.Value.ToString();
            LWindowTrackBar.ValueChanged += LWindowTrackBar_ValueChanged;

            kLabel.Text = KTrackBar.Value.ToString();
            KTrackBar.ValueChanged += KTrackBar_ValueChanged;

            DLabel.Text = DTrackBar.Value.ToString();
            DTrackBar.ValueChanged += DTrackBar_ValueChanged;

            TLabel.Text = TTrackBar.Value.ToString();
            TTrackBar.ValueChanged += TTrackBar_ValueChanged;

            button1.Enabled = false;
        }

        private void RunAlgorithm(DNA s, IEnumerable<int> minimas)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            List<DNAUtility.Pair<DNA, int>> frequentKmers = new List<DNAUtility.Pair<DNA, int>>();
            int WindowSize = WindowTrackBar.Value;
            foreach (var minima in minimas)
            {
                // Get the window of length 6000 around minima value to look for frequent kmers
                DNA window;
                if (minima - WindowSize / 2 < 0)
                {
                    int leftPartCount = WindowSize / 2 - minima;
                    DNA leftPart = DNAUtility.SubSequence(s, s.Length - leftPartCount, leftPartCount);
                    DNA rigthPart = DNAUtility.SubSequence(s, 0, minima + WindowSize / 2);
                    window = DNAUtility.ConcatenateDNA(leftPart, rigthPart);
                }
                else if (minima + WindowSize / 2 > s.Length)
                {
                    int rightPartCount = (minima + WindowSize / 2) % s.Length;
                    DNA leftPart = DNAUtility.SubSequence(s, minima - WindowSize / 2, WindowSize - rightPartCount - 1);
                    DNA rigthPart = DNAUtility.SubSequence(s, 0, rightPartCount);
                    window = DNAUtility.ConcatenateDNA(leftPart, rigthPart);
                }
                else
                {
                    window = DNAUtility.SubSequence(s, minima - WindowSize / 2, WindowSize);
                }

                Console.WriteLine("Window starts at: " + ((minima - WindowSize / 2) % s.Length) + " | Window ends at: " + ((minima + WindowSize / 2) % s.Length));
                int LWindow = LWindowTrackBar.Value;
                int k = KTrackBar.Value;
                int d = DTrackBar.Value;
                int t = TTrackBar.Value;
                var results = DNAUtility.FindKmersWithMissmatches(window, k, LWindow, d, t);

                frequentKmers.AddRange(results);
            }

            stopwatch.Stop();
            long elapsedTimeInSeconds = stopwatch.ElapsedMilliseconds / 1000;
            long elapsedTimeMiliseconds = stopwatch.ElapsedMilliseconds - elapsedTimeInSeconds * 1000;
            textBox1.Text = "Algortihm running time:" + elapsedTimeInSeconds + "s " + elapsedTimeMiliseconds + "ms" + Environment.NewLine;
            var sortedGenomes = frequentKmers.OrderByDescending(p => p.Second);
            foreach (var gen in sortedGenomes)
            {
                if (!gen.Equals(sortedGenomes.First()))
                {
                    textBox1.Text += ", " + gen.First.ToString() + "(" + gen.Second + ")";
                }
                else
                {
                    textBox1.Text += gen.First.ToString() + "(" + gen.Second + ")";
                }
            }
        }

        private void DrawChart(int [] skew, IEnumerable<int> minimas)
        {
            chart1.Series.Clear();
            chart1.Series.Add("Windows");
            chart1.Series["Windows"].ChartType = SeriesChartType.StepLine;
            chart1.Series["Windows"].ChartArea = "ChartArea1";

            foreach (var minima in minimas)
            {
                string seriesName = "Minima:" + minima;
                chart1.Series.Add(seriesName);
                chart1.Series[seriesName].ChartType = SeriesChartType.Line;
                chart1.Series[seriesName].ChartArea = "ChartArea1";
                for (int i = -40000; i < 40000; i++)
                {
                    if (i % 1000 == 0)
                    {
                        chart1.Series[seriesName].Points.Add(new DataPoint(minima, i));
                    }
                }
            }

            chart1.Series.Add("GC-Diagram");
            chart1.Series["GC-Diagram"].ChartType = SeriesChartType.Line;
            chart1.Series["GC-Diagram"].ChartArea = "ChartArea1";
            for (int i = 0; i < skew.Length; i++)
            {
                if (i % 50 == 0)
                {
                    chart1.Series["GC-Diagram"].Points.Add(new DataPoint(i, skew[i]));
                }
            }
        }

        private void WindowTrackBar_ValueChanged(object sender, EventArgs e)
        {
            WindowLengthLabel.Text = WindowTrackBar.Value.ToString();
        }
        private void LWindowTrackBar_ValueChanged(object sender, EventArgs e)
        {
            LWindowLengthLabel.Text = LWindowTrackBar.Value.ToString();
        }

        private void KTrackBar_ValueChanged(object sender, EventArgs e)
        {
            kLabel.Text = KTrackBar.Value.ToString();
        }

        private void DTrackBar_ValueChanged(object sender, EventArgs e)
        {
            DLabel.Text = DTrackBar.Value.ToString();
        }

        private void TTrackBar_ValueChanged(object sender, EventArgs e)
        {
            TLabel.Text = TTrackBar.Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int diff;
            int[] skew = DNAUtility.Skew(sequence, out diff);

            var minimas = DNAUtility.LocalMinimas(skew, skew.Length / 100, diff / 95).Distinct();

            button1.Enabled = false;
            RunAlgorithm(sequence, minimas);
            button1.Enabled = true;
        }
    }
}
