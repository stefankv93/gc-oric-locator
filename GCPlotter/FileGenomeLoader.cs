﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GCPlotter
{
    class FileGenomeLoader
    {
        public string FileLocation { get; set; }
        public DNA LoadGenome()
        {
            if (FileLocation == null) return null;

            string genomeSequece = System.IO.File.ReadAllText(FileLocation);
            return new DNA(genomeSequece);
        }

        public void SaveGenome()
        {
            throw new NotImplementedException();
        }
    }
}
