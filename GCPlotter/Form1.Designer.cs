﻿namespace GCPlotter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.WindowTrackBar = new System.Windows.Forms.TrackBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.WindowLengthLabel = new System.Windows.Forms.Label();
            this.LWindowTrackBar = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.LWindowLengthLabel = new System.Windows.Forms.Label();
            this.KTrackBar = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.kLabel = new System.Windows.Forms.Label();
            this.DTrackBar = new System.Windows.Forms.TrackBar();
            this.TTrackBar = new System.Windows.Forms.TrackBar();
            this.label = new System.Windows.Forms.Label();
            this.DLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WindowTrackBar)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LWindowTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TTrackBar)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(913, 635);
            this.chart1.TabIndex = 0;
            this.chart1.Text = "chart1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1243, 33);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(252, 30);
            this.openFileToolStripMenuItem.Text = "Load DNA";
            this.openFileToolStripMenuItem.Click += new System.EventHandler(this.openFileToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox1.Location = new System.Drawing.Point(0, 668);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(1243, 100);
            this.textBox1.TabIndex = 2;
            // 
            // WindowTrackBar
            // 
            this.WindowTrackBar.LargeChange = 100;
            this.WindowTrackBar.Location = new System.Drawing.Point(18, 36);
            this.WindowTrackBar.Maximum = 6000;
            this.WindowTrackBar.Minimum = 1000;
            this.WindowTrackBar.Name = "WindowTrackBar";
            this.WindowTrackBar.Size = new System.Drawing.Size(194, 69);
            this.WindowTrackBar.TabIndex = 3;
            this.WindowTrackBar.Value = 3000;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.TLabel);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.DLabel);
            this.panel1.Controls.Add(this.label);
            this.panel1.Controls.Add(this.DTrackBar);
            this.panel1.Controls.Add(this.TTrackBar);
            this.panel1.Controls.Add(this.kLabel);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.KTrackBar);
            this.panel1.Controls.Add(this.LWindowLengthLabel);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.LWindowTrackBar);
            this.panel1.Controls.Add(this.WindowLengthLabel);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.WindowTrackBar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(913, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(330, 635);
            this.panel1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Window length";
            // 
            // WindowLengthLabel
            // 
            this.WindowLengthLabel.AutoSize = true;
            this.WindowLengthLabel.Location = new System.Drawing.Point(164, 13);
            this.WindowLengthLabel.Name = "WindowLengthLabel";
            this.WindowLengthLabel.Size = new System.Drawing.Size(18, 20);
            this.WindowLengthLabel.TabIndex = 5;
            this.WindowLengthLabel.Text = "0";
            // 
            // LWindowTrackBar
            // 
            this.LWindowTrackBar.LargeChange = 10;
            this.LWindowTrackBar.Location = new System.Drawing.Point(18, 124);
            this.LWindowTrackBar.Maximum = 1000;
            this.LWindowTrackBar.Minimum = 100;
            this.LWindowTrackBar.Name = "LWindowTrackBar";
            this.LWindowTrackBar.Size = new System.Drawing.Size(194, 69);
            this.LWindowTrackBar.TabIndex = 6;
            this.LWindowTrackBar.Value = 500;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "L-Window length";
            // 
            // LWindowLengthLabel
            // 
            this.LWindowLengthLabel.AutoSize = true;
            this.LWindowLengthLabel.Location = new System.Drawing.Point(168, 100);
            this.LWindowLengthLabel.Name = "LWindowLengthLabel";
            this.LWindowLengthLabel.Size = new System.Drawing.Size(18, 20);
            this.LWindowLengthLabel.TabIndex = 8;
            this.LWindowLengthLabel.Text = "0";
            // 
            // KTrackBar
            // 
            this.KTrackBar.Location = new System.Drawing.Point(18, 210);
            this.KTrackBar.Maximum = 13;
            this.KTrackBar.Minimum = 4;
            this.KTrackBar.Name = "KTrackBar";
            this.KTrackBar.Size = new System.Drawing.Size(194, 69);
            this.KTrackBar.TabIndex = 9;
            this.KTrackBar.Value = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(93, 187);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "k";
            // 
            // kLabel
            // 
            this.kLabel.AutoSize = true;
            this.kLabel.Location = new System.Drawing.Point(168, 187);
            this.kLabel.Name = "kLabel";
            this.kLabel.Size = new System.Drawing.Size(18, 20);
            this.kLabel.TabIndex = 11;
            this.kLabel.Text = "0";
            // 
            // DTrackBar
            // 
            this.DTrackBar.LargeChange = 1;
            this.DTrackBar.Location = new System.Drawing.Point(18, 314);
            this.DTrackBar.Maximum = 2;
            this.DTrackBar.Name = "DTrackBar";
            this.DTrackBar.Size = new System.Drawing.Size(194, 69);
            this.DTrackBar.TabIndex = 5;
            this.DTrackBar.Value = 1;
            // 
            // TTrackBar
            // 
            this.TTrackBar.LargeChange = 1;
            this.TTrackBar.Location = new System.Drawing.Point(18, 401);
            this.TTrackBar.Minimum = 3;
            this.TTrackBar.Name = "TTrackBar";
            this.TTrackBar.Size = new System.Drawing.Size(194, 69);
            this.TTrackBar.TabIndex = 12;
            this.TTrackBar.Value = 4;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(93, 282);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(18, 20);
            this.label.TabIndex = 13;
            this.label.Text = "d";
            // 
            // DLabel
            // 
            this.DLabel.AutoSize = true;
            this.DLabel.Location = new System.Drawing.Point(168, 282);
            this.DLabel.Name = "DLabel";
            this.DLabel.Size = new System.Drawing.Size(18, 20);
            this.DLabel.TabIndex = 14;
            this.DLabel.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(93, 378);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "t";
            // 
            // TLabel
            // 
            this.TLabel.AutoSize = true;
            this.TLabel.Location = new System.Drawing.Point(164, 378);
            this.TLabel.Name = "TLabel";
            this.TLabel.Size = new System.Drawing.Size(18, 20);
            this.TLabel.TabIndex = 16;
            this.TLabel.Text = "0";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(29, 508);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(183, 46);
            this.button1.TabIndex = 17;
            this.button1.Text = "Run Algorithm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chart1);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1243, 635);
            this.panel2.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1243, 768);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "OriC Locator";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WindowTrackBar)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LWindowTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TTrackBar)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TrackBar WindowTrackBar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label WindowLengthLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LWindowLengthLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar LWindowTrackBar;
        private System.Windows.Forms.TrackBar KTrackBar;
        private System.Windows.Forms.Label kLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label TLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label DLabel;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TrackBar DTrackBar;
        private System.Windows.Forms.TrackBar TTrackBar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
    }
}

