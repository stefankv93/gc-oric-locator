﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GCPlotter.DNA;

namespace GCPlotter
{
    /// <summary>
    /// Utility class for DNA sequence manipulations
    /// </summary>
    public class DNAUtility
    {
        /// <summary>
        /// Helper class to make pair of two elements
        /// </summary>
        /// <typeparam name="T">First element</typeparam>
        /// <typeparam name="U">Second element</typeparam>
        public class Pair<T, U>
        {
            public Pair()
            {
            }

            public Pair(T first, U second)
            {
                this.First = first;
                this.Second = second;
            }

            public T First { get; set; }
            public U Second { get; set; }
        };

        /// <summary>
        /// Concatenate two DNA sequences in one
        /// </summary>
        /// <param name="g1">First DNA sequence</param>
        /// <param name="g2">Second DNA sequence which will be attached to the end of the first sequence</param>
        /// <returns>Resulting DNA</returns>
        public static DNA ConcatenateDNA(DNA g1, DNA g2)
        {
            List<Nucleotid> fullList = new List<Nucleotid>();
            fullList.AddRange(g1.DNASequence);
            fullList.AddRange(g2.DNASequence);

            return new DNA(fullList);
        }

        /// <summary>
        /// Calculates complementary nucleotid
        /// </summary>
        /// <param name="nucletiod">Input nucleotid</param>
        /// <returns>Returns complementary nucletoid of the input nucleotid</returns>
        public static Nucleotid ComplementedNucleotid(Nucleotid nucletiod)
        {
            switch (nucletiod)
            {
                case Nucleotid.A: return Nucleotid.T;
                case Nucleotid.T: return Nucleotid.A;
                case Nucleotid.C: return Nucleotid.G;
                case Nucleotid.G: return Nucleotid.C;
                default: throw new Exception("Not known nucletiod");
            }
        }

        /// <summary>
        /// Returns DNA subsequence
        /// </summary>
        /// <param name="startPosition">Starting position in the sequence</param>
        /// <param name="lenght">Length of the subsequence</param>
        /// <returns>Returns DNA subsequence</returns>
        public static DNA SubSequence(DNA sequence, int startPosition, int lenght)
        {
            return new DNA(sequence.DNASequence.GetRange(startPosition, lenght));
        }

        /// <summary>
        /// Calculates reverse complement DNA
        /// </summary>
        /// <returns>Returns reverse complementary DNA</returns>
        public static DNA ReverseComplementedDNA(DNA s)
        {
            var complementaryList = new List<Nucleotid>(s.Length);
            for (int i = s.Length - 1; i >= 0; i--)
            {
                complementaryList.Add(DNAUtility.ComplementedNucleotid(s.DNASequence[i]));
            }

            return new DNA(complementaryList);
        }

        /// <summary>
        /// Calculates Hamming distance betweeen two DNA sequences
        /// </summary>
        /// <param name="s1">First DNA sequence</param>
        /// <param name="s2">Second DNA sequence</param>
        /// <returns></returns>
        public static int HammingDistance(DNA s1, DNA s2)
        {
            if (s1.Length != s2.Length) return int.MaxValue;

            int hammingDistance = 0;

            for (int i = 0; i < s1.Length; i++)
            {
                if (s1.DNASequence[i] != s2.DNASequence[i])
                {
                    hammingDistance++;
                }
            }

            return hammingDistance;
        }

        /// <summary>
        /// Computes the value of DNA sequence in the system of base 4
        /// </summary>
        /// <returns>DNA sequence value</returns>
        public static long ToNumber(DNA s)
        {
            int value = 0;
            int multiplier = 1;
            for (int i = s.DNASequence.Count - 1; i >= 0; i--)
            {
                value += multiplier * (int)s.DNASequence[i];
                multiplier *= 4;
            }

            return value;
        }

        /// <summary>
        /// Converts number into DNA of length k(in the system of base 4)
        /// </summary>
        /// <param name="number">DNA number</param>
        /// <param name="k">Length of DNA sequence</param>
        /// <returns>Resulting DNA</returns>
        public static DNA FromNumber(long number, int k)
        {
            if (k == 1)
            {
                return new DNA(new List<Nucleotid> { (DNA.Nucleotid)number });
            }

            long reminder = number % 4;
            number = number / 4;

            DNA prefixPattern = FromNumber(number, k - 1);
            return ConcatenateDNA(prefixPattern, new DNA(new List<Nucleotid> { (DNA.Nucleotid)reminder }));
        }

        /// <summary>
        /// Calculates DNA G - C skew diagram
        /// </summary>
        /// <param name="g">Input DNA sequence</param>
        /// <param name="diff">Out difference between sku maxima and minima</param>
        /// <returns>G - C skew diagram</returns>
        public static int[] Skew(DNA g, out int diff)
        {
            int[] skewDiagram = new int[g.Length + 1];
            int min = 0;
            int max = 0;
            for (int i = 0; i < g.Length; i++)
            {
                if (g.DNASequence[i] == Nucleotid.G)
                {
                    skewDiagram[i + 1] = skewDiagram[i] + 1;
                }
                else if (g.DNASequence[i] == Nucleotid.C)
                {
                    skewDiagram[i + 1] = skewDiagram[i] - 1;
                }
                else
                {
                    skewDiagram[i + 1] = skewDiagram[i];
                }

                if (skewDiagram[i + 1] < min)
                {
                    min = skewDiagram[i + 1];
                }

                if (skewDiagram[i + 1] > max)
                {
                    max = skewDiagram[i + 1];
                }
            }

            diff = max - min;
            return skewDiagram;
        }

        /// <summary>
        /// Calculates the local minimas based on the window length and threshold parameters
        /// </summary>
        /// <param name="skew">Input skew diagram</param>
        /// <param name="Window">Window length parameter</param>
        /// <param name="tresholdValue">Threshold value parameter for choosing minimas</param>
        /// <returns>List of local minimas of the input SKU</returns>
        public static List<int> LocalMinimas(int[] skew, int Window, int tresholdValue)
        {
            List<int> minimas = new List<int>();

            int numOfIterations = (skew.Length + Window - 1) / Window;
            int fold = Window / 4;

            for (int i = 0; i < numOfIterations; i++)
            {
                int index = i == 0 ? skew.Length - fold : i * Window;
                int leftValue = skew[index];
                int rightValue = skew[(index + Window + 2 * fold) % skew.Length];
                Console.WriteLine(string.Format(
                    "Left value {0} at {1} | Right value {2} at {3}", leftValue, index, rightValue, (index + Window + 2 * fold) % skew.Length));
                int j = 0;
                int min = int.MaxValue;
                int minIndex = -1;
                while (j < Window + 2 * fold)
                {
                    if (skew[index] < min)
                    {
                        minIndex = index;
                        min = skew[index];
                    }

                    index = (index + 1) % skew.Length;
                    j++;
                }

                int minLeftRight = Math.Min(leftValue, rightValue);

                if (minLeftRight - min > tresholdValue)
                {
                    Console.WriteLine(string.Format(
                        "Found local minima {0} at {1}. Left value {2}, Right value{3}", min, minIndex, leftValue, rightValue));
                    minimas.Add(minIndex);
                }
            }

            return minimas;
        }

        /// <summary>
        /// Calculates all immidiate kmers of input kmer with maximum d number of missmatches
        /// </summary>
        /// <param name="s">Input DNA sequence</param>
        /// <param name="d">Number of missmatches</param>
        /// <param name="includeSelf">Wheater to include itself or not</param>
        /// <returns>List of immidiate kmers</returns>
        public static List<DNA> ImmediateKmers(DNA s, int d, bool includeSelf)
        {
            HashSet<DNA> set = new HashSet<DNA>() { s };

            for (int i = 0; i < s.Length; i++)
            {
                Nucleotid n = s.DNASequence[i];
                foreach (var nucleotide in new Nucleotid[] { Nucleotid.A, Nucleotid.C, Nucleotid.G, Nucleotid.T })
                {
                    if (nucleotide != n)
                    {
                        DNA immidiateKmer = new DNA(s.ToString());
                        immidiateKmer.DNASequence[i] = nucleotide;
                    }
                }
            }

            if (!includeSelf)
            {
                set.Remove(s);
            }

            return set.ToList();
        }

        /// <summary>
        /// Find all similar kmers of the input kmer with d differences
        /// </summary>
        /// <param name="s">Input DNA sequence</param>
        /// <param name="d">Number of differences</param>
        /// <param name="includeSelf">Wheter to include input sequence in the output</param>
        /// <returns>All similar kmers of the input kmer with d differencess</returns>
        public static List<DNA> SimillarKmers(DNA s, int d, bool includeSelf)
        {
            HashSet<DNA> set = new HashSet<DNA>() { s };

            for (int i = 0; i < d; i++)
            {
                HashSet<DNA> currentPatterns = new HashSet<DNA>(set);
                foreach (DNA pattern in currentPatterns)
                {
                    for (int j = 0; j < pattern.Length; j++)
                    {
                        Nucleotid symbol = pattern.DNASequence[j];
                        foreach (var n in new[] { Nucleotid.A, Nucleotid.C, Nucleotid.G, Nucleotid.T })
                        {
                            if (symbol != n)
                            {
                                DNA neighbour = new DNA(pattern.ToString());
                                neighbour.DNASequence[j] = n;
                                if (!set.Contains(neighbour))
                                {
                                    set.Add(neighbour);
                                }
                            }
                        }
                    }
                }
            }

            if (!includeSelf)
            {
                set.Remove(s);
            }

            return set.ToList();
        }

        /// <summary>
        /// Computes the frequences of each kmer in the input DNA sequence
        /// </summary>
        /// <param name="s">Input DNA sequence</param>
        /// <param name="k">Length of the kmer</param>
        /// <param name="d">Number of missmatches</param>
        /// <returns>The frequences of kmers in input DNA sequence</returns>
        public static int[] ComputeFrequencesWithMismatches(DNA s, int k, int d)
        {
            int[] currentFrequences = new int[(long)Math.Pow(4, k)];
            for (int i = 0; i <= s.Length - k; i++)
            {
                DNA kmer = SubSequence(s,i, k);
                DNA reverseKmer = ReverseComplementedDNA(kmer);
                List<DNA> simillarKmers = SimillarKmers(kmer, d, false);
                List<DNA> simillarReversKmerComplements = SimillarKmers(reverseKmer, d, true);

                int updatedSimillarKmers = 0;
                foreach (var simillarKmer in simillarKmers)
                {
                    long value = ToNumber(simillarKmer);
                    if (currentFrequences[value] == 0)
                    {
                        continue;
                    }
                    currentFrequences[value]++;
                    updatedSimillarKmers++;
                }

                foreach (var simillarReverseKmer in simillarReversKmerComplements)
                {
                    long value = ToNumber(simillarReverseKmer);
                    if (currentFrequences[value] == 0)
                    {
                        continue;
                    }
                    currentFrequences[value]++;
                    updatedSimillarKmers++;
                }

                if (currentFrequences[ToNumber(kmer)] == 0)
                {
                    currentFrequences[ToNumber(kmer)] += updatedSimillarKmers;
                }
                currentFrequences[ToNumber(kmer)]++;
            }

            return currentFrequences;
        }

        /// <summary>
        /// Finds the frequent kmers in DNA sequence
        /// </summary>
        /// <param name="s">Input DNA sequence</param>
        /// <param name="k">Lenght of the kmer</param>
        /// <param name="L">Lenght of L-Window</param>
        /// <param name="d">Number of allowed missmatchess</param>
        /// <param name="t">Number of ouccurances to filter out results</param>
        /// <returns>All the frequent kmers which frequences are great or equal to t</returns>
        public static List<Pair<DNA, int>> FindKmersWithMissmatches(DNA s, int k, int L, int d, int t)
        {
            int[] currentFrequences = new int[(long)Math.Pow(4, k)];
            int[] maxFrequences = new int[(long)Math.Pow(4, k)];

            currentFrequences = ComputeFrequencesWithMismatches(SubSequence(s, 0, L), k, d);

            for (int i = 0; i < (long)Math.Pow(4, k); i++)
            {
                maxFrequences[i] = currentFrequences[i];
            }
            // Sliding window
            for (int i = 1; i < s.Length - L; i++)
            {
                Console.WriteLine("Ukupno: " + s.Length + " / Zavrseno: " + (i + L));
                DNA removingKmer = SubSequence(s, i - 1, k);
                DNA removingKmerReverse = ReverseComplementedDNA(removingKmer);
                DNA addingKmer = SubSequence(s, i + L - 1 - k, k);
                DNA addingKmerReverse = ReverseComplementedDNA(addingKmer);

                foreach (var simillarKmer in SimillarKmers(removingKmer, d, true))
                {
                    long value = ToNumber(simillarKmer);
                    if (currentFrequences[value] == 0)
                    {
                        continue;
                    }
                    currentFrequences[value]--;
                }

                foreach (var simillarKmer in SimillarKmers(removingKmerReverse, d, true))
                {
                    long value = ToNumber(simillarKmer);
                    if (currentFrequences[value] == 0)
                    {
                        continue;
                    }
                    currentFrequences[value]--;
                }

                int updatedSimillarKmers = 0;
                foreach (var simillarKmer in SimillarKmers(addingKmer, d, false))
                {
                    long value = ToNumber(simillarKmer);
                    if (currentFrequences[value] == 0)
                    {
                        continue;
                    }
                    currentFrequences[value]++;
                    updatedSimillarKmers++;
                    if (currentFrequences[value] > maxFrequences[value])
                    {
                        maxFrequences[value] = currentFrequences[value];
                    }
                }

                foreach (var simillarKmer in SimillarKmers(addingKmerReverse, d, true))
                {
                    long value = ToNumber(simillarKmer);
                    if (currentFrequences[value] == 0)
                    {
                        continue;
                    }
                    currentFrequences[value]++;
                    updatedSimillarKmers++;
                    if (currentFrequences[value] > maxFrequences[value])
                    {
                        maxFrequences[value] = currentFrequences[value];
                    }
                }

                if (currentFrequences[ToNumber(addingKmer)] == 0)
                {
                    currentFrequences[ToNumber(addingKmer)] += updatedSimillarKmers;
                }
                currentFrequences[ToNumber(addingKmer)]++;
                if (currentFrequences[ToNumber(addingKmer)] > maxFrequences[ToNumber(addingKmer)])
                {
                    maxFrequences[ToNumber(addingKmer)] = currentFrequences[ToNumber(addingKmer)];
                }
            }

            List<Pair<DNA, int>> frequentKmers = new List<Pair<DNA, int>>();

            for (int i = 0; i < (long)Math.Pow(4, k); i++)
            {
                if (maxFrequences[i] >= t)
                {
                    Console.WriteLine("Kmer: " + FromNumber(i, k).ToString() + " / Value: " + maxFrequences[i]);
                    frequentKmers.Add(new Pair<DNA, int>(FromNumber(i, k), maxFrequences[i]));
                }
            }

            return frequentKmers;
        }

        public static int NumberOfOccurence(DNA s, DNA k)
        {
            DNA reverseComplement = ReverseComplementedDNA(k);
            List<DNA> simillarKmers = DNAUtility.SimillarKmers(k, 1, true);
            List<DNA> simillarReverse = DNAUtility.SimillarKmers(reverseComplement, 1, true);
            int count = 0;
            for (int i = 0; i <= s.Length - k.Length; i++)
            {
                var pattern = SubSequence(s, i, k.Length);
                if (simillarKmers.Contains(pattern) || simillarReverse.Contains(pattern))
                {
                    count++;
                }
            }

            return count;
        }
    }
}
